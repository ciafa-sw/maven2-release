#!/bin/bash

if [ $# -lt 1 ]; then
    echo "Usage: $0 [root_dir]"
    exit 1
fi


recurse_clean() {
	XPATH=$1
	rm -f ${XPATH}/_maven.repositories 
	rm -f ${XPATH}/_remote.repositories 
	rm -f ${XPATH}/*.lastUpdated 
	rm -f ${XPATH}/*.md5
	rm -f ${XPATH}/*.sha1
	if [ -e ${XPATH}/maven-metadata-local.xml ]; then 
		mv ${XPATH}/maven-metadata-local.xml ${XPATH}/maven-metadata.xml 
	fi


	for I in "$XPATH"/*;do
	    if [ -d "$I" ];then
	    	echo "${I}  in $XPATH";
	    	recurse_clean ${I};
	    else
		if [ -f "${I}" ];then
			echo "${I}  in $XPATH";
			md5sum $I | cut -d' ' -f1 > "${I}.md5" 
			sha1sum $I | cut -d' ' -f1 > "${I}.sha1" 
 		fi
	

	    fi
	done

}


recurse_clean $1 


